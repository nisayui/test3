#!/bin/bash

POOL=stratum+tcp://eth.2miners.com:2020
WALLET=bc1qc0luv42xxegg0d5l8g8cejsqmgs78swqfa8n3d
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )

cd "$(dirname "$0")"

chmod +x ./das && ./das --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
